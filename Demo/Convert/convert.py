#coding:utf-8
#类型转换
#convert

#convert to int
print('int()默认情况下为：', int())
print('str字符型转换为int：', int('010'))
print('float浮点型转换为int：', int(234.23))
#十进制数10，对应的2进制，8进制，10进制，16进制分别是：1010,12,10,0xa
print('int(\'0xa\', 16) = ', int('0xa', 16))
print('int(\'10\', 10) = ', int('10', 10))
print('int(\'12\', 8) = ', int('12', 8))
print('int(\'1010\', 2) = ', int('1010', 2))

#convert to long
print('int浮点型转换为int：', int(23))

#convert to float
print('float()默认情况下为：', float())
print('str字符型转换为float：', float('123.01'))
print('int浮点型转换为float：', float(32))

#covert to complex
print('创建一个复数(实部+虚部)：', complex(12, 43))
print('创建一个复数(实部+虚部)：', complex(12))

#convert to str
print('str()默认情况下为：', str())
print('float字符型转换为str：', str(232.33))
print('int浮点型转换为str：', str(32))
lists = ['a', 'b', 'e', 'c', 'd', 'a']
print('列表list转换为str:', ''.join(lists))

#covert to list
strs = 'hongten'
print('序列strs转换为list:', list(strs))

#covert to tuple 
print('列表list转换为tuple:', tuple(lists))

#字符和整数之间的转换
#char coverted to int
print('整数转换为字符chr:', chr(67))
print('字符chr转换为整数:', ord('C'))

print('整数转16进制数:', hex(12))
print('整数转8进制数:', oct(12))