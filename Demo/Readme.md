#python版本为2.7.10

#demo：
    Cdecl模式下DLL文件导入方式
    Stdcall模式下DLL文件导入方式
    Ctypes模块的试用
    文件操作示例
    类使用方法
    类型转换示例

#Example：
    列表操作
    元祖操作
    字典操作
    序列操作
    引用操作
    字符串操作

#Cr95hf：
    单block读取
    多block读取
    读取数据至文件
    
    单block写入
    多block写入
    从文件写入数据
    
    提交扇区密码
    更新扇区密码
    设置扇区权限

