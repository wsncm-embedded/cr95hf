#coding:utf-8
#Filename:python.py

'''
Created on 10-08-2015

@author: Cuihongpeng
'''

class python:

    def list(self):
    
        # 创建列表
        shoplist = ["apple", "mango", "carrot", "banana"]

        # 列表长度
        print "I have", len(shoplist), "items to purchase."
        print "These items are:",
        for item in shoplist:
            print item,
        print "\nI also have to buy rice."

        # 增加项目
        shoplist.append("rice")
        print "My shopping list is now", shoplist

        # 列表排序
        print "I will sort my list now"
        shoplist.sort()
        print "Sorted shopping list is",shoplist

        print "The first item I will buy is", shoplist[0]
        olditem=shoplist[0]

        # 删除项目
        del shoplist[0]
        print "I bought the", olditem
        print "My shopping list is now", shoplist
        
    def tuple(self):
    
        # 创建元祖
        zoo=("wolf", "elephant", "penguin")
        print "number of animals in the zoo is", len(zoo)
        
        # 元祖作为元祖的项目
        new_zoo=("monkey", "dolphin", zoo)
        
        # 元祖长度
        print "number of animals in the new zoo is", len(new_zoo)
        
        # 元祖项目
        print "all animals in new zoo are", new_zoo
        
        # 元祖访问
        print "animals brought from old zoo are", new_zoo[2]
        print "last animal brought from old zoo is", new_zoo[2][2]

    def dict(self):
    
        #创建字典
        ab = { 'Swaroop' : 'swaroopch@byteofpython.info',
        'Larry' : 'larry@wall.org',
        'Matsumoto' : 'matz@ruby-lang.org',
        'Spammer' : 'spammer@hotmail.com'
        }
        print "Swaroop's address is %s" % ab['Swaroop']
        
        # 增加键值对
        ab['Guido'] = 'guido@python.org'
        
        # 删除键值对
        del ab['Spammer']
        print '\nThere are %d contacts in the address-book\n' % len(ab)
        for name, address in ab.items():
            print 'Contact %s at %s' % (name, address)
        if 'Guido' in ab: # OR ab.has_key('Guido')
            print "\nGuido's address is %s" % ab['Guido']

    def seq(self):
    
        #创建序列
        shoplist = ['apple', 'mango', 'carrot', 'banana']
        
        # 序列索引操作
        print 'Item 0 is', shoplist[0]
        print 'Item 1 is', shoplist[1]
        print 'Item 2 is', shoplist[2]
        print 'Item 3 is', shoplist[3]
        print 'Item -1 is', shoplist[-1]
        print 'Item -2 is', shoplist[-2]
        
        # 序列切片操作
        print 'Item 1 to 3 is', shoplist[1:3]
        print 'Item 2 to end is', shoplist[2:]
        print 'Item 1 to -1 is', shoplist[1:-1]
        print 'Item start to end is', shoplist[:]
        
        # 字符串切片操作
        name = 'swaroop'
        print 'characters 1 to 3 is', name[1:3]
        print 'characters 2 to end is', name[2:]
        print 'characters 1 to -1 is', name[1:-1]
        print 'characters start to end is', name[:]

        
    def reference(self):
    
        #创建序列
        shoplist = ['apple', 'mango', 'carrot', 'banana']
        
        #引用序列，实际是指向shoplist的指针，共用一块内存空间
        mylist = shoplist
        
        #删除第一个项目，用来验证引用实际是指向同一块内存空间
        del shoplist[0]
        print 'shoplist is', shoplist
        print 'mylist is', mylist
        
        #从序列进行复制操作
        print 'Copy by making a full slice'
        mylist = shoplist[:]
        
        #删除第一个项目，用来证明复制操作与引用操作的不同
        del mylist[0] # remove first item
        print 'shoplist is', shoplist
        print 'mylist is', mylist

    def str_methods(self):
    
        # 创建字符串
        name = 'Swaroop'
        
        # 起始
        if name.startswith('Swa'):
            print 'Yes, the string starts with "Swa"'
            
        # 包含一个字符
        if 'a' in name:
            print 'Yes, it contains the string "a"'
            
        # 包含字符串
        if name.find('war') != -1:
            print 'Yes, it contains the string "war"'
        
        # 连接字符串
        delimiter = '_*_'
        mylist = ['Brazil', 'Russia', 'India', 'China']
        print delimiter.join(mylist)
        
python_test = python()

if __name__ == '__main__':
    python_test.str_methods()
    python_test.dict()
