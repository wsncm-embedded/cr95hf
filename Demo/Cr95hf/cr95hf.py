#coding:utf-8
#Filename:cr95hf.py

'''
Created on 10-08-2015

@author: Cuihongpeng
'''

import sys
import ctypes
from ctypes import * 
#from optparse import OptionParser

#load dll and get the function object
dll = ctypes.windll.LoadLibrary('cr95hf.dll');

class cr95hf:
    """
    This example is used to test the rfid by card reader.

    """

    #连接读卡器设备
    def connect(self):
        return_temp = 0;
        return_temp = dll.CR95HFDLL_USBconnect();
        if return_temp == 1:
            print ("Connect Failed!")
        else:
            print ("Connect Success!")

    # 选择ISO模式
    def select(self):
        strRequest = "010D"
        strAnswer = c_buffer(20)
        return_temp = dll.CR95HFDll_Select(strRequest, strAnswer)
        if return_temp == 1:
            print ("Select Failed!")
        else:
            print ("Select Success!")

    # 构造函数
    def __init__(self):
        self.connect()
        self.select()

    # 读取硬件版本 用于初始化是否成功的检测
    def read_hw_version(self):
        HardwareVersion = c_buffer(20)
        dll.CR95HFDLL_getHardwareVersion(HardwareVersion)
        print (HardwareVersion.raw)

    # 读取单block操作
    def single_read_block(self, addr):
        send_buff = "0A20" + addr[2:4] + addr[0:2]
        rec_buff = c_buffer(20)
        dll.CR95HFDll_SendReceive(send_buff, rec_buff)
        print ("send:"+send_buff)
        print ("read:"+rec_buff.raw)

    #读多block操作
    def multi_read_block(self, addr, len):
        rec_buff = c_buffer(20)
        for i in range(0, int(len)):
            send_buff = "0A20" + addr[2:4] + addr[0:2]
            dll.CR95HFDll_SendReceive(send_buff, rec_buff)
            addr = str(hex(int(addr, 16) + 1)[2:5]).zfill(4)
            print ("send:"+send_buff)
            if rec_buff.raw[0:6] == '800800':
                print (addr+':'+rec_buff.raw[6:14])
            else:
                print (addr+': read failed')

    #读取数据到txt文本
    def read_data_to_file(self, addr, len, file_name):
        f = open(file_name,'w')
        rec_buff = c_buffer(20)
        for i in range(0, int(len)):
            send_buff = "0A20" + addr[2:4] + addr[0:2]
            dll.CR95HFDll_SendReceive(send_buff, rec_buff)
            addr = str(hex(int(addr, 16) + 1)[2:5]).zfill(4)
            print ("send:"+send_buff)
            if rec_buff.raw[0:6] == '800800':
                print (addr+':'+rec_buff.raw[6:14])
                f = open(file_name,'a')
                f.write(addr+':'+rec_buff.raw[6:14]+'\n')
            else:
                print (addr+': read failed')
                f = open(file_name,'a')
                f.write(addr+': read failed\n')

    #写单block操作
    def single_write_block(self, addr, data):
        send_buff = "0A21" + addr[2:4] + addr[0:2] + data
        rec_buff = c_buffer(20)
        dll.CR95HFDll_SendReceive(send_buff,rec_buff)
        print ("send:"+send_buff)
        print ("read:"+rec_buff.raw)

    #多block写入
    def multi_write_block(self, addr, len, data):
        rec_buff = c_buffer(20)
        for i in range(0, int(len)):
            send_buff = "0A21" + addr[2:4] + addr[0:2] + data
            dll.CR95HFDll_SendReceive(send_buff, rec_buff)
            addr = str(hex(int(addr, 16) + 1)[2:5]).zfill(4)
            print ("send:"+send_buff)
            if rec_buff.raw[0:6] == '800400':
                print (addr + ': write success!')
            else:
                print (addr + ': write falied!')

                
    #从文件写入数据
    def write_from_file(self, addr, len, file_name):
        rec_buff = c_buffer(20)
        f = open(file_name,'r')
        receive_data = f.read()
        for i in range(0, int(len)):
            send_buff = "0A21" + addr[2:4] + addr[0:2] + receive_data[i*9:i*9+8]
            dll.CR95HFDll_SendReceive(send_buff, rec_buff)
            addr = str(hex(int(addr, 16) + 1)[2:5]).zfill(4)
            print ("send:"+send_buff)
            if rec_buff.raw[0:6] == '800400':
                print (addr + ': write success!')
            else:
                print (addr + ': write falied!')
                
    #提交密码
    def present_password(self, num, password):
        send_buff = "02B302" + num + password
        rec_buff = c_buffer(20)
        dll.CR95HFDll_SendReceive(send_buff,rec_buff)
        if rec_buff.raw[0:6] == "800400":
            print ("present password success!")
        else:
            print ("present password failed!")
        #print ("send:"+send_buff)
        #print ("read:"+rec_buff.raw)

    #更新密码
    def write_password(self, num, password):
        send_buff = "02B102" + num + password
        rec_buff = c_buffer(20)
        dll.CR95HFDll_SendReceive(send_buff,rec_buff)
        if rec_buff.raw[0:6] == "800400":
            print ("write password success!")
        else:
            print ("write password failed!")
        #print ("send:"+send_buff)
        #print ("read:"+rec_buff.raw)

    #设置权限 value:0F/09/15/1D
    #0F 密码1保护本扇区，未提交密码时不能读写，提交密码后只能读不能写
    #09 密码1保护本扇区，未提交密码时不能读写，提交密码后可读可写
    #15 密码2保护本扇区，未提交密码时不能读写，提交密码后可读可写
    #1d 密码3保护本扇区，未提交密码时不能读写，提交密码后可读可写
    #权限设置不可以修改
    def lock_sector(self, sector, value):
        sector = hex(int(sector)*32)[2:6].zfill(4)
        send_buff = "0AB202" + sector[2:4] + sector[0:2] + value
        rec_buff = c_buffer(20)
        dll.CR95HFDll_SendReceive(send_buff,rec_buff);
        print ("send:"+send_buff)
        print ("read:"+rec_buff.raw)

def work():
    option = sys.argv[1]
    rfid = cr95hf()
    # 初始化
    if option == '-i':
        # rfid.init();
        rfid.read_hw_version();
    # 单block读取
    elif option == '-r':
        # rfid.init();
        rfid.single_read_block(sys.argv[2]);
    #多block读取
    elif option == '-mr':
        # rfid.init();
        rfid.multi_read_block(sys.argv[2], sys.argv[3]);
    #读取数据到文件
    elif option == '-fr':
        # rfid.init();
        rfid.read_data_to_file(sys.argv[2], sys.argv[3], sys.argv[4]);
    #单block写入
    elif option == '-w':
        # rfid.init();
        rfid.single_write_block(sys.argv[2], sys.argv[3]);
    #多block写入
    elif option == '-mw':
        # rfid.init();
        rfid.multi_write_block(sys.argv[2], sys.argv[3], sys.argv[4]);
    #从文件写入
    elif option == '-fw':
        # rfid.init();
        rfid.write_from_file(sys.argv[2], sys.argv[3], sys.argv[4]);
    #提交密码
    elif option == '-pp':
        # rfid.init();
        rfid.present_password(sys.argv[2], sys.argv[3]);
    #写入密码
    elif option == '-wp':
        # rfid.init();
        rfid.write_password(sys.argv[2], sys.argv[3]);
    #设置权限
    elif option == '-sp':
        # rfid.init();
        rfid.lock_sector(sys.argv[2], sys.argv[3]);
    #帮助
    elif option == '-h':
        print ("-i to init, e.g.: python cr95hf.py -i ")
        print ("-r to single block read, e.g.: python cr95hf.py -r '0030' ")
        print ("-mr to multi block read, e.g.: python cr95hf.py -mr '0030' '10' ")
        print ("-fr to read data to file, e.g.: python cr95hf.py -fr '0030' '10' 'read_data.txt'")
        print ("-w to single block write, e.g.: python cr95hf.py -w '0030' '11223344' ")
        print ("-mw to multi block write, e.g.: python cr95hf.py -mw '0030' '10' '11223344' ")
        print ("-fw to multi block write, e.g.: python cr95hf.py -mw '0030' '10' 'write_data.txt' ")
        print ("-pp to present password, e.g.: python cr95hf.py -pp '01' '00000000' ")
        print ("-wp to write password, e.g.: python cr95hf.py -pp '01' '11111111' ")
        #见lock_sector函数描述
        print ("-sp to set access, e.g.: python cr95hf.py -sp '63' '09' ")
    #错误参数
    else:
        print ("Illegal parameter, for help please input -h, e.g.: python cr95hf.py -h")

if __name__ == "__main__":
    work()